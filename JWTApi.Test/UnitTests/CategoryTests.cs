using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;

namespace JWTApi.Test
{
    [TestClass]
    public class CategoryTests
    {
        //would be better to use route reverse
        public static string categoriesUrl = "https://localhost:5001/api/v1/categories";
        public static string productsUrl = "https://localhost:5001/api/v1/products";
        public string tokenUrl = "https://localhost:5001/api/login";
        
        public static Uri categoriesUri;
        public static Uri productsUri;
        public static HttpClient client;
        public static string tokenJwt;
        public TestContext TestContext { get; set; }

        [ClassInitialize]
        public static void Setup(TestContext tc){
            client = new HttpClient();
            categoriesUri = new Uri(categoriesUrl);
            productsUri = new Uri(productsUrl);
        }

        [ClassCleanup]
        public static void TearDown(){
            client.Dispose();
        }

        [TestMethod]
        public void LoginGetTokenInvalidUser()
        {
            var payload = new {Username = "carlos", Password = "123"};
            HttpContent payloadContent = new StringContent(
                JsonConvert.SerializeObject(payload), 
                Encoding.UTF8, 
                "application/json"
            );
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(
                HttpMethod.Post, 
                new Uri(tokenUrl)) { Content = payloadContent };

            var response = client.SendAsync(httpRequestMessage).Result;
            Assert.AreEqual((int)response.StatusCode, 404);
        }

        [TestMethod]
        public void LoginGetTokenValidUserAndPassword()
        {
            var payload = new {Username = "joao", Password = "123"};
            HttpContent payloadContent = new StringContent(
                JsonConvert.SerializeObject(payload), 
                Encoding.UTF8, 
                "application/json"
            );
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(
                HttpMethod.Post, 
                new Uri(tokenUrl)) { Content = payloadContent };

            var response = client.SendAsync(httpRequestMessage).Result;
            Task<string> responseData = response.Content.ReadAsStringAsync();
            JObject jsonToken = JObject.Parse(responseData.Result);
            tokenJwt = (string)jsonToken["token"];
            Assert.AreEqual((int)response.StatusCode, 200);
        }

        [TestMethod]
        public void CategoryGetAllStatusCode200()
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(
                HttpMethod.Get, 
                new Uri(categoriesUrl));
            httpRequestMessage.Headers.Add("Authorization", String.Format("Bearer {0}", tokenJwt));

            Task<HttpResponseMessage> httpResponse = client.SendAsync(httpRequestMessage);
            HttpResponseMessage httpResponseMessage = httpResponse.Result;
            Assert.AreEqual((int)httpResponseMessage.StatusCode, 200);
        }

        //using context exemple
        [TestMethod]
        [TestProperty("prop", "true")]
        [TestProperty("prop2", "false")]
        public void CategoryGetAllStatusCode200UsingContext()
        {
            //var pars = TestContext.Properties;
            //foreach (DictionaryEntry prop in TestContext.Properties){
            //    Console.WriteLine(prop.Value);
            //}
        
            using (HttpClient httpClient = new HttpClient()){
                using (HttpRequestMessage httpRequestMessage = new HttpRequestMessage()){
                    httpRequestMessage.Method = HttpMethod.Get;
                    httpRequestMessage.RequestUri = categoriesUri;
                    httpRequestMessage.Headers.Add("Accept", "application/json");
                    httpRequestMessage.Headers.Add("Authorization", String.Format("Bearer {0}", tokenJwt));
                    
                    Task<HttpResponseMessage> httpResponse = client.SendAsync(httpRequestMessage);

                    using (HttpResponseMessage httpResponseMessage = httpResponse.Result){
                        Task<string> responseData = httpResponseMessage.Content.ReadAsStringAsync();
                        Assert.AreEqual((int)httpResponseMessage.StatusCode, 200);
                    }
                }
            }
        }

        [TestMethod]
        public void CategoryPostOneContent()
        {
            var payload = new {  Title = "First Category" };
            HttpContent payloadContent = new StringContent(
                JsonConvert.SerializeObject(payload), 
                Encoding.UTF8, 
                "application/json"
            );
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(
                HttpMethod.Post, 
                categoriesUri) { Content = payloadContent };
            httpRequestMessage.Headers.Add("Authorization", String.Format("Bearer {0}", tokenJwt));

            var response = client.SendAsync(httpRequestMessage).Result;
            Assert.AreEqual((int)response.StatusCode, 200); //normally it had to be 201
        }

    }
}
