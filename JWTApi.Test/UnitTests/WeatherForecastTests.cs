using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace JWTApi.Test
{
    [TestClass]
    public class WeatherForecastTests
    {
        //would be better to use route reverse
        public static string weatherUrl = "https://localhost:5001/api/v1/weatherforecast";
        public static Uri weatherUri;

        [ClassInitialize]
        public static void Setup(TestContext tc){
            weatherUri = new Uri(weatherUrl);
        }

        [TestMethod]
        public void WeatherForecastGetFive()
        {
            using (HttpClient httpClient = new HttpClient()){
                using (HttpRequestMessage httpRequest = new HttpRequestMessage()){
                    httpRequest.Method = HttpMethod.Get;
                    httpRequest.RequestUri = weatherUri;
                    httpRequest.Headers.Add("Accept", "application/json");
                    
                    Task<HttpResponseMessage> httpResponse = httpClient.SendAsync(httpRequest);

                    using (HttpResponseMessage httpResponseMessage = httpResponse.Result){
                        Task<string> responseData = httpResponseMessage.Content.ReadAsStringAsync();
                        Console.WriteLine(responseData);
                        Assert.AreEqual((int)httpResponseMessage.StatusCode, 200);
                    }
                }
            }
        }

    }
}
