using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using JWTApi.Controllers.Helpers;

namespace JWTApi.Test
{
    [TestClass]
    public class PasswordGenerator3Tests
    {
        [AssemblyInitialize]
        public static void BeforeAssembly(TestContext tc){
            Console.WriteLine("Executed before assembly");
        }

        [AssemblyCleanup]
        public static void AfterAssembly(){
            Console.WriteLine("Executed after assembly");
        }

        [ClassInitialize]
        public static void BeforeClass(TestContext tc){
            Console.WriteLine("Executed before class");
        }

        [ClassCleanup]
        public static void AfterClass(){
            Console.WriteLine("Executed after class");
        }

        [TestInitialize]
        public void BeforeTests(){
            Console.WriteLine("Executed before test");
        }

        [TestCleanup]
        public void AfterTests(){
            Console.WriteLine("Executed after test");
        }

        [TestMethod, Priority(1), TestCategory("Password")]
        public void PasswordGeneratorEightEquals()
        {
            var passwordGenerator = new PasswordGenerator();
            var password = passwordGenerator.Generate();
            Assert.AreEqual(password.Length, 8, $"Password length was {password.Length} and should have been 8");
        }

        [TestMethod]
        [Description("Exceptions")]
        [DataRow(-1)]
        [DataRow(-2)]
        public void PasswordGeneratorNegativeLengthThrowException(int quantity){
            var passwordGenerator = new PasswordGenerator(quantity);
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => passwordGenerator.Generate());
        }

        [TestMethod]
        [Ignore] //Needs refactoring
        public void _PasswordGeneratorNegativeLengthThrowException(){
            var passwordGenerator = new PasswordGenerator(-1);
            Assert.ThrowsException<ArgumentOutOfRangeException>(() => passwordGenerator.Generate());
        }
    }
}
