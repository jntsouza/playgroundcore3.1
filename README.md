## Challenge
Project done by Jonathan de Souza Bastos

### Goal
Test some .Net Core 3.1 features

### 📕 Documentation
You can see and test APIs by documentation on Postman, click [HERE](https://documenter.getpostman.com/view/1302324/UVknsbBk).

### 📋 Requirements
* .Net Core 3.1
* Docker 19.03.1+
* docker-compose 1.24.1+

### 🚀 Getting started
1. Open a terminal and Clone the github code ``` git clone https://gitlab.com/jntsouza/playgroundcore3.1.git ```
2. Navigate to playgroundcore3.1 folder ``` cd playgroundcore3.1 ```
4. Run ``` docker-compose up ``` to run the created docker imagem
5. On a new terminal, run these commands:
``` 
    cd JWTApi
    dotnet ef database update
```
6. To execute the tests, run these commands with application running, it will generate log and coverage xml (it does not cover Http tests):
```
   cd ..
   cd JWTApi.Test
   dotnet test --collect:"XPlat Code Coverage" -l "console;verbosity=detailed" --logger trx 
```
7. Execute to generate coverage chart:
```
   reportgenerator -reports:"./TestResults/{guid}/coverage.cobertura.xml" -targetdir:"coveragereport" -reporttypes:Html 
```
