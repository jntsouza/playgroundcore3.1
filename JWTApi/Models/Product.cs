using System.ComponentModel.DataAnnotations;


namespace JWTApi.Models 
{
    public class Product
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [MaxLength(60, ErrorMessage = "Max. 60")]
        [MinLength(3, ErrorMessage = "Min. 3")]
        public string Title { get; set; }

        [MaxLength(1024, ErrorMessage = "Max. 1024")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [Range(1, int.MaxValue, ErrorMessage = "Maior que zero")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        [Range(1, int.MaxValue, ErrorMessage = "Categoria inválida")]
        public int CategoryId { get; set; }
        public Category Category { set; get; }
    }
}
