using System.Collections.Generic;

namespace JWTApi.Models
{
    public class UserConstants
    {
       public static List<UserModel> Users = new List<UserModel>()
       {
            new UserModel() {
               Username = "joao", EmailAddress = "joao@hello.world", Password = "123",
               Role = "Administrator"
            },
            new UserModel() {
               Username = "maria", EmailAddress = "maria@hello.world", Password = "123",
               Role = "Employee"
            },
       };
    }
}
