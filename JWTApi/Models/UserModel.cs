using System.ComponentModel.DataAnnotations;


namespace JWTApi.Models
{
    public class UserModel
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(60, ErrorMessage = "Este campo é obrigatório")]
        [MinLength(3, ErrorMessage = "Este campo é obrigatório")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Este campo é obrigatório")]
        public string Password { get; set; }
        public string EmailAddress { get; set; }
        public string Role { get; set; }
    }
}
