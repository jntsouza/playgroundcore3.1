using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JWTApi.Data;
using JWTApi.Models;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace JWTApi.Controllers
{
    [ApiController]
    [Route("api/v1/products")]
    [Authorize(Roles = "Administrator,Employee")]
    public class ProductController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Product>>> GetAsync([FromServices] DataContext context){
            //filter by user obj domain
            var currentUser = new UserController().GetCurrentUser(this.HttpContext);
            var products = await context.Products.Include(x => x.Category).ToListAsync(); // TODO: see Include: probably select_related
            return products;
        }

        [HttpGet]
        [Route("{id:int}")]
        public async Task<ActionResult<Product>> GetById([FromServices] DataContext context, int id){
            var product = await context.Products.Include(x => x.Category)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);
            return product;
        }

        [HttpGet]
        [Route("categories/{id:int}")]
        public async Task<ActionResult<List<Product>>> GetByCategoryAsync([FromServices] DataContext context, int id){
            var products = await context.Products
                .Include(x => x.Category)
                .AsNoTracking()
                .Where(x => x.CategoryId == id)
                .ToListAsync(); //Always last
            return products;
        }

        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<Product>> PostAsync(
            [FromServices] DataContext context,
            [FromBody] Product model){
            
            if (ModelState.IsValid){
                context.Products.Add(model);
                await context.SaveChangesAsync();
                return model;
            }
            else {
                return BadRequest(ModelState);
            }
        }
    }

}