using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using JWTApi.Data;
using JWTApi.Models;
using Microsoft.AspNetCore.Authorization;

namespace JWTApi.Controllers
{
    [ApiController]
    [Route("api/v1/categories")]
    [Authorize(Roles = "Administrator,Employee")]
    public class CategoryController : ControllerBase
    {
        [HttpGet]
        [Route("")]
        public async Task<ActionResult<List<Category>>> Get([FromServices] DataContext context){
            var categories = await context.Categories.ToListAsync();
            return categories;
        }

        [HttpPost]
        [Route("")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult<Category>> Post(
            [FromServices] DataContext context,
            [FromBody] Category model){
            
            if (ModelState.IsValid){
                context.Categories.Add(model);
                await context.SaveChangesAsync();
                return model;
            }
            else {
                return BadRequest(ModelState);
            }
        }
    }
}
